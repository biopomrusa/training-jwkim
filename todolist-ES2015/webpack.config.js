'use strict';


var pkg = require('./package.json');
var webpack = require('webpack');
var SafeUmdPlugin = require('safe-umd-webpack-plugin');
var isProduction = process.argv.indexOf('--production') >= 0;
var babelPolyfill = require('babel-polyfill');
var PolyfillsPlugin = require('webpack-polyfills-plugin');
var es3ifyPlugin = require('es3ify-webpack-plugin');



var FILENAME = pkg.name + (isProduction ? '.min.js' : '.js');
var BANNER = [
    FILENAME,
    '@version ' + pkg.version,
    '@author ' + pkg.author,
    '@license ' + pkg.license
].join('\n');

var config = {
    //devtool: 'source-map',
    entry: ['babel-polyfill', './src/js/index.js'],
    output: {
        filename: './dist/' + FILENAME,
        libraryTarget: 'umd',
        library: ['Todo']
    }, module: {
        preLoaders: [
            {
                test: /\.js$/,
                exclude: /(dist|node_modules)/,
                loader: 'eslint-loader'
            }
        ],
        loaders: [
            {
                test: /\.js$/,
                exclude: /(dist|node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: ["es2015"]
                }
            }

        ]
    },
    plugins: [
        new SafeUmdPlugin(),
        new webpack.BannerPlugin(BANNER),
        new es3ifyPlugin()
    ]
};

if (isProduction) {
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        compress: {
            'screw_ie8': false
        }
    }));
}

module.exports = config;
