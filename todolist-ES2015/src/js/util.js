
export default class Util {
    /**
     * add Class
     * @param {HTMLElement} element - target element
     * @param {string} className - class name
     */
    static addClass(element, className) {
        element.className += ' ' + className;
    }
    /**
     * add Class
     * @param {HTMLElement} element = target element
     * @param {string} className - class name
     */
    static removeClass(element, className) {
        let origClassName = element.className;
        let re = new RegExp('\\s?' + className + '\\s?', 'g');
        element.className = origClassName.replace(re, '');
    }
    /**
     * find element target
     * @param {EventObject} event - eventObject
     * @returns {HTMLElement} eventTarget = target element
     */
    static findEventTarget(event) {
        let eventTarget = null;
        event = event || window.event;
        eventTarget = event.target || event.srcElement;

        return eventTarget;
    }
    /**
     * html template function
     * @param {string} template - template target string
     * @param {Object} setting - setting value object
     * @returns {string} template - maked new template
     */
    static template(template, setting) {
        let matchList = template.match(/{\$([^}]+)}/g);
        if (matchList) {
            Util.forEach(matchList, function(replaceTarget) {
                let findSettingName = replaceTarget.replace(/^\{\$|\}/g, '');
                template = template.replace(replaceTarget, setting[findSettingName]);
            });
        }

        return template;
    }
    /**
     * foreach util
     * @param {string} targetList - template target string
     * @param {Function} callback - loop callback
     */
    static forEach(targetList, callback) {
        let length = targetList.length;
        let i = 0;
        for (; i < length; i += 1) {
            callback(targetList[i], i);
        }
    }
    /**
     * addEventHandler
     * @param {HTMLElement} element - template target string
     * @param {string} type - loop callback
     * @param {Function} handler - handler function
     */
    static addEventHandler(element, type, handler) {
        if (element.addEventListener) {
            element.addEventListener(type, handler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + type, handler);
        } else {
            element['on' + type] = handler;
        }
    }
}

